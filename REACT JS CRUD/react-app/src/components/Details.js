import { useState, useEffect, useContext } from "react";
import { Container, Card,  Row, Col, Form } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import React from 'react';

export default function Details() {

	  const { familyId } = useParams();

	  const [firstName, setFirstName] = useState("");
	  const [lastName, setLastName] = useState("");
	  const [email, setEmail] = useState("");
	  const [gender, setGender] = useState("");
	  const [age, setAge] = useState("");
	  const [birthdate, setBirthdate] = useState("");
	  const [mobileNo, setMobileNo] = useState("");

	  useEffect(() => {

	    fetch(`${process.env.REACT_APP_API_URL}/family/${familyId}`)
	      .then((res) => res.json())
	      .then((data) => {

	        setFirstName(data.firstName);
	        setLastName(data.lastName);
	        setEmail(data.email);
	        setGender(data.gender);
	        setAge(data.age);
	        setBirthdate(data.birthdate);
	        setMobileNo(data.mobileNo);
	        
	      });
	  }, [familyId]);

return (
  <Container fluid className="py-5">
    <Row className="py-5">
      <Col className="col-6 offset-3 py-5">
      <h5 className="text-center fw-bold">Family Member Details</h5>
        <Card>
          <Card.Body className="text-center font">
            <Card.Title>{firstName}{lastName}</Card.Title>
            <Card.Subtitle>Gender: </Card.Subtitle>
            <Card.Text>{gender}</Card.Text>
            <Card.Subtitle>Age:</Card.Subtitle>
            <Card.Text>{age}</Card.Text>
            <Card.Subtitle>Email:</Card.Subtitle>
            <Card.Text>{email}</Card.Text>
            <Card.Subtitle>Mobile No:</Card.Subtitle>
            <Card.Text>{mobileNo}</Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  </Container>
);
}

