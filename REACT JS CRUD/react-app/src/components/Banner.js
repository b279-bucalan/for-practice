import { Row, Col, Button} from "react-bootstrap"
import { Link } from "react-router-dom"

export default function Banner({bannerProps}){

	const{title, content, destination, label} = bannerProps;

	return(
		<div className="">
			<Row>
				<Col className="">
					<h1 className="home-title pb-5">{title}</h1>
					<p className="p-title">{content}</p>

					<Link to={destination}>
                    <Button size="large" variant="contained" className="fw-bold">{label}</Button>
                	</Link>
					
				</Col>
			</Row>
		</div>
		);
}