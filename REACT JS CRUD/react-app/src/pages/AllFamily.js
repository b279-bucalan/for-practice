import { useContext, useState, useEffect } from "react";
// import UserContext from "../UserContext";
import { Navigate, Link } from "react-router-dom";
import {
  Container,
  Col,
  Row,
  Button,
  Table,
  Modal,
  Form,
  Image,
} from "react-bootstrap";
import Swal from "sweetalert2";
import React from "react";
import Details from "../components/Details.js";
// import "./AllProduct.css"

export default function AllProducts() {
  const [allFamily, setAllFamily] = useState([]);
  const [familyId, setFamilyId] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [age, setAge] = useState("");
  const [birthdate, setBirthdate] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [modalAdd, setModalAdd] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);

  const addFamilyOpen = () => setModalAdd(true);
  const addFamilyClose = () => setModalAdd(false);

  const openEdit = (id) => {
    setFamilyId(id);

    fetch(`${process.env.REACT_APP_API_URL}/family/${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setGender(data.gender);
        setAge(data.age);
        setBirthdate(data.birthdate);
        setMobileNo(data.mobileNo);
      });

    setModalEdit(true);
  };

  const closeEdit = () => {
    setFamilyId("");
    setFirstName("");
    setLastName("");
    setEmail("");
    setGender("");
    setAge("");
    setBirthdate("");
    setMobileNo("");

    setModalEdit(false);
  };

  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/family/allFamily`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setAllFamily(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const archive = (id, name) => {
    Swal.fire({
      title: "Confirmation",
      text: `Are you sure you want to delete ${name}?`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes",
      cancelButtonText: "No",
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/family/${id}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            isActive: false,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            console.log(data);

            if (data) {
              Swal.fire({
                title: "FAMILY MEMBER DELETING SUCCESS!",
                icon: "success",
                text: "Successful",
              });

              fetchData();
            } else {
              Swal.fire({
                title: "Delete Unsuccessful",
                icon: "error",
                text: "Something went wrong. Please try again later",
              });
            }
          });
      }
    });
  };

  const addFamily = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/family/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        gender: gender,
        age: age,
        birthdate: birthdate,
        mobileNo: mobileNo,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "FAMILY MEMBER ADDED SUCCESSFULLY!",
            icon: "success",
            text: "Family member added.",
          });

          fetchData();
          addFamilyClose();
        } else {
          Swal.fire({
            title: "ADD FAMILY MEMBER UNSUCCESSFUL!",
            icon: "error",
            text: "The system is experiencing trouble at the moment. Please try again later.",
          });
          addFamilyClose();
        }
      });

    setFirstName("");
    setLastName("");
    setEmail("");
    setGender("");
    setAge("");
    setBirthdate("");
    setMobileNo("");
  };

  const editFamily = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/family/${familyId}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        gender: gender,
        age: age,
        birthdate: birthdate,
        mobileNo: mobileNo,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "FAMILY INFO EDIT SUCCESSFUL!",
            icon: "success",
            text: "Family info was edited successfully.",
          });

          fetchData();
          closeEdit();
        } else {
          Swal.fire({
            title: "FAMILY INFO EDIT UNSUCCESSFUL!",
            icon: "error",
            text: "The system is experiencing trouble at the moment. Please try again later.",
          });

          closeEdit();
        }
      });

    setFamilyId("");
    setFirstName("");
    setLastName("");
    setEmail("");
    setGender("");
    setAge("");
    setBirthdate("");
    setMobileNo("");
  };

  return (
    <>
      <Container>
        <Row className="m-5 p-5">
          <Col className="d-flex justify-content-center">
            <div className="bg-white rounded p-5 border border-dark all-font">
              <div className="my-2 text-center">
                <h1 className="fw-bold">FAMILY MEMBERS</h1>
              </div>
              <div className="my-2 text-right">
                <Button
                  variant="primary"
                  className="px-3 fw-bold"
                  onClick={addFamilyOpen}
                >
                  Add Family Member
                </Button>
              </div>
              <Table striped bordered hover className="text-center">
                <thead className="banner-bg text-dark">
                  <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile No</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {allFamily.map((family) => (
                    <tr key={family._id}>
                      <td>{family._id}</td>
                      <td>{family.firstName}</td>
                      <td>{family.lastName}</td>
                      <td>{family.email}</td>
                      <td>{family.mobileNo}</td>
                      {/*<td>{family.isFamily}</td>*/}
                      <td>
                        <Link to={`/Details/${family._id}`}>
                          <Button variant="primary" className="" size="sm">
                            Details
                          </Button>
                        </Link>
                        <Button
                          variant="secondary"
                          className="mx-1"
                          size="sm"
                          onClick={() => openEdit(family._id)}
                        >
                          Edit
                        </Button>
                        <Button
                          variant="danger"
                          size="sm"
                          onClick={() => archive(family._id, family.firstName)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          </Col>
        </Row>
      </Container>

      <Modal
        className="all-font"
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={modalAdd}
      >
        <Form onSubmit={(e) => addFamily(e)}>
          <Modal.Header className="banner-bg">
            <Modal.Title className="fw-bold">Add Family Member</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>
              <Col xs={12} md={12} lg={12}>
                <Form.Group className="mb-3">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder=""
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Gender</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={gender}
                    onChange={(e) => setGender(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Age</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={age}
                    onChange={(e) => setAge(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Birthdate</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={birthdate}
                    onChange={(e) => setBirthdate(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Mobile No</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="primary" type="submit" id="submitBtn">
              Add Family
            </Button>
            <Button variant="secondary" onClick={addFamilyClose}>
              Close
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

      <Modal
        className="all-font"
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered
        show={modalEdit}
      >
        <Form onSubmit={(e) => editFamily(e)}>
          <Modal.Header className="banner-bg">
            <Modal.Title className="fw-bold">
              Edit Family Member Info
            </Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Row>
              <Col xs={12} md={12} lg={12}>
                <Form.Group className="mb-3">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder=""
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Gender</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={gender}
                    onChange={(e) => setGender(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Age</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={age}
                    onChange={(e) => setAge(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Birthdate</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={birthdate}
                    onChange={(e) => setBirthdate(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Mobile No</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder=""
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="primary" type="submit" id="submitBtn">
              Save
            </Button>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
