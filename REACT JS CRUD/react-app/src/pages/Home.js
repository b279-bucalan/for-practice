import Banner from "../components/Banner"

export default function Home(){

	const data = {
	title: "Family Tree Maker",
	content: "Make your family tree here!",
	destination: "/family/allFamily",
	label: "Button"
	}

return(
			<>
			<div className="container-fluid py-5 homepage">
				<div className="row py-5 my-5 justify-content-start">
					<div className="col-7 offset-md-2 py-5 my-5">
						<Banner bannerProps={data} />
						{/*<Highlights/>*/}
						
					</div>
				</div>
			</div>
			</>
		)
}