import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import AllFamily from './pages/AllFamily';
import Details from "./components/Details"

// Dependencies
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"

function App() {
  return (
  <Router>

    <AppNavbar/>
      <Routes>
          <Route exact path="/" element={<AllFamily/>} />
          <Route path="/Details/:familyId" element={<Details/>}/>
      </Routes>

  </Router>
  );
}

export default App;
