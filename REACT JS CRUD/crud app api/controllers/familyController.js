const Family = require("../model/Family");

module.exports.registerUser = (reqBody) => {

	let newFamily = new Family({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		gender : reqBody.gender,
		age : reqBody.age,
		birthdate : reqBody.birthdate,
		mobileNo: reqBody.mobileNo

		
	})

	return newFamily.save().then((user, error) => {
		if(error) {
			return false;
		}else {
			return true;
		}
	})

};

module.exports.getAllFam = () => {
	return Family.find({}).then(result =>{
		return result;
	})
}

// Get specific family member
module.exports.getFamily = (reqParams) => {
	return Family.findById(reqParams.familyId).then(result =>{
		return result;
	})
}

module.exports.updateFamily = (reqParams, reqBody) => {
	
		let updatedFamily = {
			firstName : reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			gender : reqBody.gender,
			age : reqBody.age,
			birthdate : reqBody.birthdate,
			mobileNo: reqBody.mobileNo,
	}
		return Family.findByIdAndUpdate(reqParams.familyId, updatedFamily).then((family, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
	})

}

module.exports.deleteFam = (reqParams, reqBody) => {
	
	return Family.findByIdAndDelete(reqParams.familyId, reqBody).then((family, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


