const mongoose = require("mongoose");
const familySchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : true
	},
	lastName : {
		type : String,
		required : true
	},
	email : {
		type : String,
		required : true
	},
	gender : {
		type : String,
		required : true
	},
	age : {
		type: Number,
		required : true
	},
	birthdate : {
		type : String,
		required : true
	},
	mobileNo: {
		type : String,
		required : true
	}
	

})

module.exports = mongoose.model("Family", familySchema);
