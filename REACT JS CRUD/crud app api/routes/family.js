const express = require("express");
const router = express.Router();
const familyController = require("../controllers/familyController.js")

// Add a family
router.post("/register", (req, res) =>{ 
	familyController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Retrieve all family
router.get("/allFamily",(req,res)=> {
	// console.log('test')
	familyController.getAllFam().then(resultFromController => res.send(resultFromController));
})

// Retrieve specific family
router.get('/:familyId',(req, res)=>{
	console.log(req.params.familyId);
	familyController.getFamily(req.params).then(resultFromController => res.send(resultFromController));
})

// Update specific family
router.put('/:familyId', (req, res)=>{
	familyController.updateFamily(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// delete a specific family
router.delete('/:familyId', (req, res)=>{
	familyController.deleteFam(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;